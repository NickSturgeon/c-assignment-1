/* PROGRAM:             search.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/10/23
   PURPOSE:             Search functions for students
   LEVEL OF DIFFICULTY: 2
   CHALLENGES:          Figuring out to use a char array instead of pointer
   HOURS SPENT:         1.5 hours
*/

#include <stdio.h>
#include "search.h"
#include "student.h"
#include "phone_number.h"

/* Prompts for an area code and 7-digit phone number
 * and searches for a student with a matching number.
 * If one is found, displays their information. Otherwise,
 * says that no match was found. */
void search_phone_num(void)
{
	short area_code;
	long phone_number;
	struct student * students = get_students();
	int i, arr_length = num_students();
	int match_found = 0;

	printf("Enter area code: ");
	scanf("%d", &area_code);

	printf("Enter 7-digit phone number: ");
	scanf("%ld", &phone_number);
	printf("\n");

	for (i = 0; i < 10; i++) {
		if (area_code == students[i].area_code 
				&& phone_number == students[i].phone_number) {
			match_found = 1;
			print_details(students[i]);
		}
	}

	if (!match_found) printf("No Match Found\n");

	printf("\n");
	while (getchar() != '\n') continue; /* clear buffer */
}

/* Searches for every student whose area code matches
 * the one provided. If none are found, display that no
 * match was found. */
void search_area_code(void)
{
	short area_code;
	struct student * students = get_students();
	int i, arr_length = num_students();
	int match_found = 0;

	printf("Enter area code: ");
	scanf("%d", &area_code);
	printf("\n");

	for (i = 0; i < 10; i++) {
		if (area_code == students[i].area_code) {
			match_found = 1;
			print_details(students[i]);
		}
	}

	if (!match_found) printf("No Match Found\n");

	printf("\n");
	while (getchar() != '\n') continue; /* clear buffer */
}

/* Searches for a student whose last name matches
 * the one provided. If none are found, display that no
 * match was found. */
void search_last_name(void)
{
	char last_name[256];
	struct student * students = get_students();
	int i, arr_length = num_students();
	int match_found = 0;

	printf("Enter last name: ");
	scanf("%s", last_name);
	printf("\n");

	for (i = 0; i < 10; i++) {
		if (strcmp(last_name, students[i].last_name) == 0) {
			match_found = 1;
			print_details(students[i]);
		}
	}

	if (!match_found) printf("No Match Found\n");

	printf("\n");
	while (getchar() != '\n') continue; /* clear buffer */
}
