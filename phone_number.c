/* PROGRAM:             phone_number.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/10/23
   PURPOSE:             Functions relating to students' phone numbers
   LEVEL OF DIFFICULTY: 2
   CHALLENGES:          Allocating memory to return a pointer
   HOURS SPENT:         2 hours
*/

#include <stdio.h>
#include <stdlib.h>
#include "phone_number.h"

#define PHONE_STR_LEN 15

struct area_code area_codes[] = {
/* Area Code, City Name */
	{ 613, "Ottawa"  },
	{ 416, "Toronto" },
	{ 647, "Toronto" },
	{ 519, "Windsor" },
	{ 905, "Niagara" }
};

/* Format an area code and 7-digit phone number as a 10-digit
 * phone number with format (###) ###-####.

 * area_code - area code of phone number
 * phone_number - 7-digit portion of phone number */
char * format_phone_number(short area_code, long phone_number)
{
	char * formatted_phone_number = (char *) malloc(PHONE_STR_LEN);
	short central_office = phone_number / 10000;
	short subscriber = phone_number % 10000;

	snprintf(formatted_phone_number, PHONE_STR_LEN, 
		"(%d) %d-%04d", area_code, central_office, subscriber
	);

	return formatted_phone_number;
}

/* Return a pointer to the area codes array. */
struct area_code * get_area_codes(void)
{
	return area_codes;
}

/* Return the size of the area codes array. */
int num_area_codes()
{
	return sizeof(area_codes) / sizeof(struct area_code);
}

/* Match a student's area code to one in the array.
 * 
 * area_code - student's area code to match in the system
 *
 * Returns the name of the city associated with the area code,
 * or "Unknown City" if none are found. */
char * match_area_code(short area_code)
{
	int i, arr_length = num_area_codes();
	for (i = 0; i < arr_length; i++) {
		if (area_code == area_codes[i].area_code)
			return area_codes[i].name;
	}

	return "Unknown City";
}

/* Display the area code and respective city for each area code
 * in the array. */
void display_area_codes(void)
{
	struct area_code * area_codes = get_area_codes();
	int i, arr_length = num_area_codes();

	printf("\nAll Area Codes in system:\n");

	for (i = 0; i < arr_length; i++) {
		printf("Area Code %d belongs to city %s\n", 
			area_codes[i].area_code, area_codes[i].name);
	}

	printf("\n");
}
