/* PROGRAM:             main.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/10/23
   PURPOSE:             Main entry point of the program
   LEVEL OF DIFFICULTY: 1
   CHALLENGES:          None
   HOURS SPENT:         5 minutes
*/

#include "menu.h"

/* Main entry point to the program. */
int main(void)
{
	display_menu();
	return 0;
}
