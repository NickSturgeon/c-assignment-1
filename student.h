#ifndef STUDENT_H
#define STUDENT_H 

struct student {
	char * first_name;
	char * last_name;
	short area_code;
	long phone_number;
};

/* Function Prototypes */
void print_details(struct student);
struct student * get_students(void);
int num_students(void);

#endif
