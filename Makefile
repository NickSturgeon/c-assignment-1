CC = gcc
CC_FLAGS = -pedantic -Wall -w -ansi
OUT_EXE = assignment1
FILES = menu.c student.c phone_number.c search.c main.c

build: $(FILES)
	$(CC) $(CC_FLAGS) -o $(OUT_EXE) $(FILES)

clean: 
	rm -rf assignment1

rebuild: clean build
