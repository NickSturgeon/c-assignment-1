/* PROGRAM:             student.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/10/23
   PURPOSE:             Functions relating to a student
   LEVEL OF DIFFICULTY: 2
   CHALLENGES:          Learning how to use structs
   HOURS SPENT:         1.75 hours
*/

#include <stdio.h>
#include <stdlib.h>
#include "student.h"
#include "phone_number.h"

struct student students[] = {
/* First Name, Last Name, Area Code, 7-Digit Phone Number */
	{ "Roger",   "Stanley",  613, 5781237 },
	{ "Bobby",   "Filmore",	 416, 8905649 },
	{ "Stan",    "Raddin",	 519, 2275677 },
	{ "Alyssa",  "Lisner",	 647, 3416993 },
	{ "Craig",   "Lowne",	 647, 6680912 },
	{ "Candace", "Sinclair", 613, 3266891 },
	{ "Joey",    "Stitz",	 519, 8552381 },
	{ "Luke",    "Brown",	 613, 6434315 },
	{ "Isabel",  "Morowitz", 905, 2454664 },
	{ "Lilly",   "Langsum",	 905, 7967623 },
};

/* Return a pointer to the students array. */
struct student * get_students(void)
{
	return students;
}

/* Return the size of the students array. */
int num_students(void)
{
	return sizeof(students) / sizeof(struct student);
}

/*  Print the details of a student.
 *	s - struct of the student to display */
void print_details(struct student s)
{
	char * city = match_area_code(s.area_code);
	char * phone_number = format_phone_number(s.area_code, s.phone_number);

	printf("Phone number: %s belongs to the student %s, %s and their number is from %s\n",
		phone_number, s.last_name, s.first_name, city
	);

	free(phone_number);
}
