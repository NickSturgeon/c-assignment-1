/* PROGRAM:             menu.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/10/23
   PURPOSE:             Loop the menu of the program
   LEVEL OF DIFFICULTY: 1
   CHALLENGES:          None
   HOURS SPENT:         15 minutes
*/

#include <stdio.h>
#include <string.h>
#include "menu.h"
#include "search.h"
#include "phone_number.h"

/* Loop a menu until the user quits and perform various
 * function based on their menu choice. */
void display_menu(void)
{
	char choice;

	for(;;) {
		printf("PHONE NUMBER DIRECTORY\n");
		printf("1: Get information based on PHONE NUMBER\n");
		printf("2: Get information based on AREA CODE\n");
		printf("3: Get information based on LAST NAME\n");
		printf("4: Print all area code information\n");
		printf("Q: QUIT\n");
		printf("\nEnter choice: ");

		scanf("%c", &choice);
		while (getchar() != '\n') continue; /* clear buffer */

		switch (choice) {
			case '1' :
				search_phone_num();
				break;
			case '2' :
				search_area_code();
				break;
			case '3' :
				search_last_name();
				break;
			case '4' :
				display_area_codes();
				break;
			case 'q' :
			case 'Q' :
				printf("Quitting...\n");
				return;
			default :
				printf("Invalid Option\n\n");
				break;
		}
	}
}
