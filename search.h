#ifndef SEARCH_H
#define SEARCH_H

/* Function Prototypes */
void search_phone_num(void);
void search_area_code(void);
void search_last_name(void);

#endif