#ifndef PHONE_NUMBER_H
#define PHONE_NUMBER_H

struct area_code {
	short area_code;
	char * name;
};

/* Function Prototypes */
char * format_phone_number(short, long);
struct area_code * get_area_codes(void);
int num_area_codes(void);
char * match_area_code(short);
void display_area_codes(void);

#endif
